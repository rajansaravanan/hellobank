package com.hellobank.service;

import com.hellobank.HellobankApplication;
import com.hellobank.entity.Account;
import com.hellobank.repo.InMemoryAccountRepo;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = HellobankApplication.class)
public class TransferServiceTest {
    @Resource
    private TransferService transferService;

    @Resource
    private InMemoryAccountRepo inMemoryAccountRepo;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @After
    public void tearDown() {
        inMemoryAccountRepo.removeAllAccount();
    }

    @Test
    public void transferAmount_SuccessScenario() throws Exception {
        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        Account account2 = new Account(2);
        account2.setBalance(101.00);

        inMemoryAccountRepo.addAccount(account1);
        inMemoryAccountRepo.addAccount(account2);

        //when
        transferService.transferMoney(account1.getId(), account2.getId(), 50.00);

        //then
        assertThat(account1.getId()).isEqualTo(1);
        assertThat(account1.getBalance()).isEqualTo(50.00);
        assertThat(account2.getId()).isEqualTo(2);
        assertThat(account2.getBalance()).isEqualTo(151.00);
    }

    @Test
    public void transferAmount_SuccessScenario_2() throws Exception {
        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        Account account2 = new Account(2);
        account2.setBalance(101.00);

        inMemoryAccountRepo.addAccount(account1);
        inMemoryAccountRepo.addAccount(account2);

        //when
        transferService.transferMoney(account1.getId(), account2.getId(), 100.00);

        //then
        assertThat(account1.getId()).isEqualTo(1);
        assertThat(account1.getBalance()).isEqualTo(0);
        assertThat(account2.getId()).isEqualTo(2);
        assertThat(account2.getBalance()).isEqualTo(201.00);
    }

    @Test
    public void transferAmount_FailureScenarioWhenInsufficientFundsInSourceAccount() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Insufficient funds in source account.");

        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        Account account2 = new Account(2);
        account2.setBalance(101.00);

        inMemoryAccountRepo.addAccount(account1);
        inMemoryAccountRepo.addAccount(account2);

        //when
        transferService.transferMoney(account1.getId(), account2.getId(), 101.00);

    }

    @Test
    public void transferAmount_FailureScenarioWhenSourceAccountNotFound() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Source Account not found.");

        //given
        Account accountNotInMemory = new Account(1);

        Account account2 = new Account(2);
        account2.setBalance(100.00);

        inMemoryAccountRepo.addAccount(account2);

        //when
        transferService.transferMoney(accountNotInMemory.getId(), account2.getId(), 50.00);
    }

    @Test
    public void transferAmount_FailureScenarioWhenTargetAccountNotFound() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Target Account not found.");

        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        inMemoryAccountRepo.addAccount(account1);

        Account accountNotInMemory = new Account(2);

        //when
        transferService.transferMoney(account1.getId(), accountNotInMemory.getId(), 50.00);
    }

    @Test
    public void transferAmount_FailureScenarioWhenSourceAccountNull() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Source Account not found.");

        //given
        Account account2 = new Account(2);
        account2.setBalance(100.00);

        inMemoryAccountRepo.addAccount(account2);

        //when
        transferService.transferMoney(null, account2.getId(), 50.00);
    }

    @Test
    public void transferAmount_FailureScenarioWhenSourceAndTargetAccountNull() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Source Account not found.");

        //when
        transferService.transferMoney(null, null, 50.00);
    }

    @Test
    public void transferAmount_FailureScenarioWhenSTransferAmountNull() throws Exception {
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Transfer Amount cannot be 0 or in minus value.");

        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        Account account2 = new Account(2);
        account2.setBalance(101.00);

        inMemoryAccountRepo.addAccount(account1);
        inMemoryAccountRepo.addAccount(account2);

        //when
        transferService.transferMoney(account1.getId(), account2.getId(), null);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
}