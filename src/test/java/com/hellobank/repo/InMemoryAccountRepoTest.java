package com.hellobank.repo;

import com.hellobank.HellobankApplication;
import com.hellobank.entity.Account;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HellobankApplication.class)
public class InMemoryAccountRepoTest {

    @Resource
    private InMemoryAccountRepo inMemoryAccountRepo;

    @After
    public void setup() {
        inMemoryAccountRepo.removeAllAccount();
    }

    @Test(expected = Exception.class)
    public void addNullAccount() throws Exception {
        //given
        inMemoryAccountRepo.addAccount(new Account(null));

        //when
        Account account1 = inMemoryAccountRepo.getAccount(null);

        //then

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(0);
    }

    @Test(expected = Exception.class)
    public void addNullAccountNumber() throws Exception {
        //given
        inMemoryAccountRepo.addAccount(new Account(null));

        //when
        Account account1 = inMemoryAccountRepo.getAccount(null);

        //then

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(0);
    }

    @Test
    public void addValidAccounts() throws Exception {
        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        Account account2 = new Account(2);
        account2.setBalance(101.00);

        inMemoryAccountRepo.addAccount(account1);
        inMemoryAccountRepo.addAccount(account2);

        //when
        Account account1FromRepo = inMemoryAccountRepo.getAccount(1);
        Account account2FromRepo = inMemoryAccountRepo.getAccount(2);

        //then
        assertThat(account1FromRepo.getId()).isEqualTo(1);
        assertThat(account1FromRepo.getBalance()).isEqualTo(100.00);

        assertThat(account2FromRepo.getId()).isEqualTo(2);
        assertThat(account2FromRepo.getBalance()).isEqualTo(101.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(2);
    }

    @Test
    public void removeValidAccount() throws Exception {
        //given
        Account account1 = new Account(1);
        account1.setBalance(100.00);

        Account account2 = new Account(2);
        account2.setBalance(101.00);

        inMemoryAccountRepo.addAccount(account1);
        inMemoryAccountRepo.addAccount(account2);

        Account account1FromRepo = inMemoryAccountRepo.getAccount(1);
        Account account2FromRepo = inMemoryAccountRepo.getAccount(2);
        assertThat(account1FromRepo.getId()).isEqualTo(1);
        assertThat(account1FromRepo.getBalance()).isEqualTo(100.00);
        assertThat(account2FromRepo.getId()).isEqualTo(2);
        assertThat(account2FromRepo.getBalance()).isEqualTo(101.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(2);

        //when
        inMemoryAccountRepo.removeAccount(1);

        //then
        Account account1AfterRemove = inMemoryAccountRepo.getAccount(1);

        //then
        assertThat(account1AfterRemove).isNull();

        assertThat(account2FromRepo.getId()).isEqualTo(2);
        assertThat(account2FromRepo.getBalance()).isEqualTo(101.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(1);

    }

    @Test
    public void addAmountToValidAccount() throws Exception {
        //given
        Account account = new Account(1);
        account.setBalance(100.00);
        inMemoryAccountRepo.addAccount(account);

        Account account1 = inMemoryAccountRepo.getAccount(1);
        assertThat(account1.getId()).isEqualTo(1);
        assertThat(account1.getBalance()).isEqualTo(100.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(1);

        //when
        inMemoryAccountRepo.addAmountToAccount(1, 101.00);

        //then
        assertThat(account1.getBalance()).isEqualTo(201.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(1);
    }

    @Test
    public void deductAmountFromValidAccount() throws Exception {
        //given
        Account account = new Account(1);
        account.setBalance(100.00);
        inMemoryAccountRepo.addAccount(account);

        Account account1 = inMemoryAccountRepo.getAccount(1);
        assertThat(account1.getId()).isEqualTo(1);
        assertThat(account1.getBalance()).isEqualTo(100.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(1);

        //when
        inMemoryAccountRepo.deductAmountFromAccount(1, 99.00);

        //then
        assertThat(account1.getBalance()).isEqualTo(1.00);

        assertThat(inMemoryAccountRepo.getTotalAccounts()).isEqualTo(1);
    }


}