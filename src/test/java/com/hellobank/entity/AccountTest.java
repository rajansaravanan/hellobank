package com.hellobank.entity;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {
    @Test
    public void testAccount() {
        Account account = new Account(1);
        account.setBalance(100.00);

        assertThat(account.getId()).isEqualTo(1);
        assertThat(account.getBalance()).isEqualTo(100d);

    }
}
