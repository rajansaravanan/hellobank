package com.hellobank;

import com.hellobank.service.TransferService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import com.hellobank.repo.InMemoryAccountRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HellobankApplication.class)
public class HellobankApplicationTests {

	@Test
	public void contextLoads() {
	}

}
