package com.hellobank.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hellobank.service.TransferService;
import com.hellobank.vo.TransferVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class TransferControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TransferService transferService;

    @Test
    public void testTransferSuccess()
            throws Exception {

        TransferVo transferVo = new TransferVo();
        transferVo.setSourceAccountNumber(1);
        transferVo.setTargetAccountNumber(2);
        transferVo.setTransferAmount(100.00);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(transferVo);

        doNothing().when(transferService).transferMoney(1, 2, 100.00);

        mvc.perform(post("/api/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk());

        verify(transferService, times(1)).transferMoney(1, 2, 100.00);
    }


    @Test
    public void testTransferFailure()
            throws Exception {

        TransferVo transferVo = new TransferVo();
        transferVo.setSourceAccountNumber(1);
        transferVo.setTargetAccountNumber(2);
        transferVo.setTransferAmount(100.00);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(transferVo);

        doThrow(new Exception()).when(transferService).transferMoney(1, 2, 100.00);

        mvc.perform(post("/api/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isExpectationFailed());

        verify(transferService, times(1)).transferMoney(1, 2, 100.00);
    }
}