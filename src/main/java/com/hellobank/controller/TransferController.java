package com.hellobank.controller;

import com.hellobank.service.TransferService;
import com.hellobank.vo.TransferVo;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class TransferController {

    @Resource
    private TransferService transferService;


    @RequestMapping(method = POST, value="/api/transfer", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> transferMoney(@RequestBody TransferVo transferVo)  {
        try {
            transferService.transferMoney(transferVo.getSourceAccountNumber(), transferVo.getTargetAccountNumber(), transferVo.getTransferAmount());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Error " + e.getMessage());
        }
        System.out.println("" + transferVo);
        return ResponseEntity.status(HttpStatus.OK).body("Success");
    }
}


