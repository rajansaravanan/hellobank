package com.hellobank.entity;

public class Account {
    private final Integer id;
    private Double balance;

    private Account() {
        this.id = null;
    }

    public Account(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
