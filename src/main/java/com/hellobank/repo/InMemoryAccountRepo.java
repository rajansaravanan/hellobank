package com.hellobank.repo;

import com.hellobank.entity.Account;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Repository
public class InMemoryAccountRepo {
    private final Map<Integer, Account> accountMap = new HashMap<>();

    public void addAccount(Account account) throws Exception {
        checkValidAccount(account);

        accountMap.put(account.getId(), account);
    }

    public void removeAccount(Integer accountNumber) throws Exception {
        checkValidAccountNumber(accountNumber);

        accountMap.remove(accountNumber);
    }

    public Account getAccount(Integer accountNumber) {
        return accountMap.get(accountNumber);
    }

    public void addAmountToAccount(Integer accountNumber, double amount) {
        Account account = accountMap.get(accountNumber);
        account.setBalance(account.getBalance() + amount);
    }

    public void deductAmountFromAccount(Integer accountNumber, double amount) {
        Account account = accountMap.get(accountNumber);
        account.setBalance(account.getBalance() - amount);
    }

    public int getTotalAccounts() {
        return accountMap.size();
    }

    public void removeAllAccount() {
        accountMap.clear();;
    }

    private void checkValidAccount(Account account) throws Exception {
        if (Objects.isNull(account) || checkInValidAccountNumber(account.getId())) {
            throw new Exception("Invalid account details");
        }
    }

    private boolean checkInValidAccountNumber(Integer accountNumber) {
        if (Objects.isNull(accountNumber)) {
            return true;
        }

        return false;
    }


    private void checkValidAccountNumber(Integer accountNumber) throws Exception {
        if (checkInValidAccountNumber(accountNumber)) {
            throw new Exception("Invalid account details");
        } else if(Objects.isNull(accountMap.get(accountNumber))) {
            throw new Exception("Account not found");
        }
    }
}
