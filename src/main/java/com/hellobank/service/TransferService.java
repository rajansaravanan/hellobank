package com.hellobank.service;

import com.hellobank.entity.Account;
import com.hellobank.repo.InMemoryAccountRepo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TransferService {

    @Resource
    private InMemoryAccountRepo inMemoryAccountRepo;

    public void transferMoney(Integer sourceAccountNumber, Integer targetAccountNumber, Double transferAmount) throws Exception{
        Account sourceAccount = inMemoryAccountRepo.getAccount(sourceAccountNumber);
        if(sourceAccount==null) {
            throw new Exception("Source Account not found.");
        }

        Account targetAccount = inMemoryAccountRepo.getAccount(targetAccountNumber);
        if(targetAccount==null) {
            throw new Exception("Target Account not found.");
        }

        if(transferAmount==null || transferAmount.compareTo(Double.valueOf(0.00))<=0) {
            throw new Exception("Transfer Amount cannot be 0 or in minus value.");
        }

        if(sourceAccount.getBalance().compareTo(transferAmount)<=-1) {
            throw new Exception("Insufficient funds in source account.");
        }

        sourceAccount.setBalance(sourceAccount.getBalance()-transferAmount);
        targetAccount.setBalance(targetAccount.getBalance()+transferAmount);
    }
}
