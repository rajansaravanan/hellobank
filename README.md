# Transfer Money between two accounts.

## How to start

The application can be built by gradle wrapper command:

    ./gradlew build

The above will fetch dependencies and run all tests

To run the app use following command:

    ./gradlew bootRun

The application will start on the default port 8080


## Transfer Service Rest API
URL:  http://localhost:8080/api/transfer
Method:  POST
Data Input as Json and example:
        {
          "sourceAccountNumber" : 1,
          "targetAccountNumber" : 2,
          "transferAmount" : 100.0
        }

## Other dependencies and concerns
I have implemented the TransferService rest api and covered mostly all test scenarios.  However for the application to work as a whole, we need an AccountService rest api to create accounts before we can do money transfers between accounts.